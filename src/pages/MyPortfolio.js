import React from 'react';

import NavBar from '../components/NavBar';
import Portfolio from '../components/Portfolio';

import '../App.css';

export default function MyPortfolio(){
	return (
		<>
			<NavBar/>
			<Portfolio/>
			{/*
			<Highlights/>
			*/}
		</>
		)
}
