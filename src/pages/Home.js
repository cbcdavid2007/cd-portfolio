import React from 'react';

import NavBar from '../components/NavBar';
import Banner from '../components/Banner';
import Portfolio from '../components/Portfolio';
import ContactMe from '../components/ContactMe';
import Footer from '../components/Footer';
import Highlights from '../components/Highlights';

import '../App.css';

export default function Home(){
	return (
		<>
			<NavBar/>
			<Banner/>
			{/*
			<Highlights/>
			*/}
			<hr className="featurette-divider"/>			
			<Portfolio/>
			<hr className="featurette-divider"/>
			<ContactMe/>
			<hr className="featurette-divider"/>
			<Footer/>															
		</>
		)
}
