import React from 'react';

import NavBar from '../components/NavBar';
import ContactMe from '../components/ContactMe';

import '../App.css';

export default function Contact(){
	return (
		<>
			<NavBar/>
			<ContactMe/>
			{/*
			<Highlights/>
			*/}
		</>
		)
}
