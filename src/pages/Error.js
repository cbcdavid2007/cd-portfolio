import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron'
import {Container} from 'react-bootstrap'
import {Button} from 'react-bootstrap'

import '../App.css';

export default function Error(){
	const errorData = {
		title: "404 - Page Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home Page"
	}

	return(
	<Jumbotron className="Jumbotron d-flex align-items-center mb-0">
		<Container className="text-center">
			<h1> 404 - Page Not Found </h1>
			<h6> The page you are looking for cannot be found </h6>
		    <Button href="/" className="headerButton mt-4" size="lg"> Back to Home Page</Button>
		</Container>
	</Jumbotron>
		)
}
