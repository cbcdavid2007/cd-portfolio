import React from 'react';
import {Row} from 'react-bootstrap'
import {Col} from 'react-bootstrap'
import Card from 'react-bootstrap/Card'
import '../App.css';

export default function Highlights(){
	return(
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title> Learn From Home </Card.Title>
						<Card.Text> Lorem ipsum, dolor sit amet consectetur. </Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title> Study Now, Pay Later </Card.Title>
						<Card.Text> Lorem ipsum, dolor sit amet consectetur. </Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title> Be Part of Our Community </Card.Title>
						<Card.Text> Lorem ipsum, dolor sit amet consectetur. </Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		)
}