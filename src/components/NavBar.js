import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

import '../App.css';

export default function NavBar(){
	return(
		<Navbar expand="md" className="Header sticky-top">
			<Navbar.Brand href="#"></Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav" className="justify-content-center">
				<Nav activeKey="/">
					{/*
					<Nav.Item>
					  <Nav.Link href="/">Home</Nav.Link>
					</Nav.Item>
					*/}
					<Nav.Item>
					  <Nav.Link href="/" className = "NavLink">Home</Nav.Link>
					</Nav.Item>
					<Nav.Item>
					  <Nav.Link href="/aboutMe" className = "NavLink">About Me</Nav.Link>
					</Nav.Item>
					{/*
					<Nav.Item>
					  <Nav.Link href="#" className = "NavLink">Skills</Nav.Link>
					</Nav.Item>
					*/}
					<Nav.Item>
					  <Nav.Link href="/portfolio" className = "NavLink">Portfolio</Nav.Link>
					</Nav.Item>
					<Nav.Item>
					  <Nav.Link href="/contactMe" className = "NavLink">Contact Me</Nav.Link>
					</Nav.Item>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		)
}