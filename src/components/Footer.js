import React from 'react';
import '../App.css';

export default function Footer(){
	return(
	<footer className="container end-text pb-5">
	  <p className="float-right"><a href="/" className = "contactLink">Back to top</a></p>
	  <p>&copy; 2020 Christine David | Portfolio </p>
	</footer>
	)
}
