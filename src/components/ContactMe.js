import React from 'react';
import {Container} from 'react-bootstrap'
import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import {Button} from 'react-bootstrap'

import contactPic from '../images/contactPic.png'

import '../App.css';

export default function ContactMe(){
	return(
		<section className="my-5 container-fluid" id= "ContactMe">

		  <div className="row align-items-center">
		    
		    <div className = "col-md-1">
		    </div>
		    <div className="col-md-4 mb-5 text-center">
		      <img src={contactPic} className = "contact" alt="..."/>
		    </div>
		    <div className="col-md-6 mb-5 text-center">
		      <h2 className="h2-responsive font-weight-bold text-center my-3 help-you">
		        How can I help you? <span className="text-muted">Contact me. </span>
		      </h2>
		      <div className="list-unstyled mb-0 text-center my-3 contact-info">
		        <p className = "contact-details"> <i className="fas fa-envelope pr-2"></i> <a href="mailto:cbcdavid2007@gmail.com" target="_blank" rel="noreferrer" className = "contactLink">cbcdavid2007@gmail.com</a></p>
		        <p className = "contact-details"> <i className="fab fa-linkedin pr-2"></i> <a href="https://www.linkedin.com/in/ccdavid3/" target="_blank" rel="noreferrer" className = "contactLink">ccdavid3</a></p>
		      </div>  
		    </div>

		  </div>

		</section>

		)
}