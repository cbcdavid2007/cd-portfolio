import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron'
import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import {Container} from 'react-bootstrap'
import {Button} from 'react-bootstrap'

import '../App.css';

export default function Banner(){
	return(
				<Jumbotron className="Jumbotron d-flex align-items-center mb-0" id="AboutMe">
					<Container className="text-center">
						<h3 className = "subheading3"> HELLO. I'M </h3>
						<h1 className = "cd"> CHRISTINE DAVID </h1>
					    <p className = "subheading"> Full Stack Web and Software Developer</p>
					    <Button href="#ContactMe" className="headerButton mt-2" size="lg"> Contact me.</Button>
					</Container>
				</Jumbotron>
		)
}