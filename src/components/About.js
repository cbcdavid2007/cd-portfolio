import React from 'react';
import {Container} from 'react-bootstrap'
import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import {Button} from 'react-bootstrap'

import project1 from '../images/Project2.png'
import project2 from '../images/4.png'
import project3 from '../images/Project3WIP.png'
import '../App.css';

import aboutPic from '../images/Tin_ID.jpg'

export default function About(){
	return(
		<>

		<div className="container marketing" id="AboutMe">
		  <div className="row featurette">
		    <div className="col-lg-4 order-md-1 pic-side">
		      <img className="img-fluid your-pic" src={aboutPic} alt="..."/>
		    </div>          
		    <div className="col-lg-8 order-md-3 about-me-section">
		      <h1 className="christine-david"><span className="text-muted">I'm </span>Christine David.</h1>
		      <p className="lead">A Full Stack Web and Software Developer</p>
		      <div className = "about-p">
		          <p>
		            I am a <strong> full stack web and software developer </strong> with experience in creating <strong> static, dynamic, and integrated apps </strong>. I have knowledge in HTML, CSS, Bootstrap, Javascript, Node, MongoDB, React, Next, RUP, Chart.js, SQL, and Python. I have successfully deployed an accounting <strong>course booking system</strong> with register, login, logout, enroll, and profile features plus admin specifications capable of adding, updating, archiving, and deleting courses. I have also successfully deployed a <strong> budget tracking app </strong> that uses data charts to monitor and visualize personal and business expenses. Generally, I am a <strong> creative, analytical, and data-driven </strong> solution provider committed to solve complex business problems.
		          </p>
		          <p>
		            <strong> Plus feature:</strong> I have a background as a business analyst and project manager in finance and accounting, making it easier for me to understand your business processes, needs and requirements.
		          </p> 
		          <p>
		            <strong>Another plus feature:</strong> I have experience in data and analytics where I extract, transform, and load data to create <strong> interactive dashboards </strong> making it easier for me to discuss data-driven business strategies and actionable insights to management. 
		          </p>
		          <p>
		            <strong> Education background:</strong> I graduated with flying colors from the top university in my country, the University of the Philippines, with a double degree in Business Administration and Accountancy.
		          </p>
		      </div>
		    </div>
		  </div>
		</div>

		</>
		)
}