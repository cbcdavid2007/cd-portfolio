import React from 'react';
import {Container} from 'react-bootstrap'
import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import {Button} from 'react-bootstrap'

import project1 from '../images/Project2.png'
import project2 from '../images/4.png'
import project3 from '../images/BUDGETBUDDY_1.png'
import '../App.css';

export default function Portfolio(){
	return(
		<>
			<Container id="MyPortfolio" className="my-5">
				<Row className="text-center">
					<Col md={12}>
						<h1 className="text-center mb-5 help-you"> My Portfolio</h1>
					</Col>
				</Row>
			{/*start of Project 1*/}
			  <div>
			    <Row className = "text-center headerProj">
			      <Col md={12}>
			        <h1> Static Website</h1>
			        <p className="smaller"> <strong> Portfolio: </strong> Full Stack Web and Software Developer</p>
			        <hr className = "mb-4"/> 
			      </Col>
			    </Row>

			    <div className = "row text-center">
			      <div className = "col-lg-3 side-info2">
			        <div className="container text-left">
			            <p className = "mb-2 mt-2"> 
			              <strong> Project Title:</strong> <br/> Christine David | Portfolio
			            </p>
			            <p className = "mb-2">
			              <strong> Project Description:</strong> A static portfolio website to showcase skills and projects as a full stack web and software developer
			            </p>
			            <p className = "mb-2">
			              <strong>Features:</strong> <br/>
			              <span className="badge badge-secondary">landing</span>
			              <span className="badge badge-secondary">about me</span>
			              <span className="badge badge-secondary">skills</span> <br/>
			              <span className="badge badge-secondary">projects</span>
			              <span className="badge badge-secondary">contact info</span>
			            </p>
			            <p className = "mb-2">                                       
			              <strong> Additional Feature: </strong><br/>
			              <span className="badge badge-secondary">responsiveness</span>
			            </p>
			            <p className = "mb-2">
			              <strong> Try it out on these devices: </strong><br/>                         
			              <span className="badge badge-secondary">desktop</span>                    
			              <span className="badge badge-secondary">mobile</span>                    
			              <span className="badge badge-secondary">tablet</span>                    
			            </p>
			            <p className = "mb-2">
			              <strong>Tools used:</strong> <br/>
			              <span className="badge badge-secondary">HTML</span>
			              <span className="badge badge-secondary">CSS</span>
			              <span className="badge badge-secondary">Bootstrap</span>
			              <span className="badge badge-secondary">Gitlab</span>
			            </p>                  
			            <a className = "my-3 btn btn-block btn-dark btn-sm" href="/" target="_blank" rel="noreferrer">Check project here</a>                
			        </div>
			      </div>

			      <div className = "col imageSide">
			        <a target="_blank" href="/" rel="noreferrer"><img src={project1} className="card-img imageProject" alt="..."/></a> 
			      </div>
			    </div>
			  </div>
			{/*end of Project 1*/}

			{/*start of Project 2*/}
			  <div className = "mt-5">
			    <div className = "row text-center headerProj">
			      <div className = "col-md-12 order-md-2">
			        <h1> Dynamic App</h1>
			        <p className="smaller"> <strong> Accounting PH: </strong>A Pinoy Beginner's Guide to Accounting</p>
			        <hr className = "mb-4"/>
			      </div>
			    </div>

			    <div className = "row text-center">
			      <div className = "col-lg-3 order-md-1 side-info2">
			        <div className="container text-left">
			            <p className = "mb-2 mt-2">
			              <strong> Project Title:</strong> Accounting PH
			            </p>
			            <p className = "mb-2">
			              <strong> Project Description:</strong> An accounting course booking system for Filipino beginners wanting to learn accounting
			            </p>
			            <p className = "mb-2">
			              <strong>User Features:</strong> <br/>
			              <span className="badge badge-secondary">landing</span>
			              <span className="badge badge-secondary">register</span>
			              <span className="badge badge-secondary">login</span> <br/>
			              <span className="badge badge-secondary">logout</span>
			              <span className="badge badge-secondary">enroll</span>
			              <span className="badge badge-secondary">profile</span>
			            </p>
			            <p className = "mb-2">            
			              <strong> Admin Features: </strong> <br/>
			              <span className="badge badge-secondary">add course</span>                    
			              <span className="badge badge-secondary">get course</span>                    
			              <span className="badge badge-secondary">update course</span>                    
			              <span className="badge badge-secondary">archive course</span>                    
			              <span className="badge badge-secondary">delete course</span>                    
			              <span className="badge badge-secondary">check enrollees</span>                    
			            </p>
			            <p className = "mb-2">
			              <strong>Tools used:</strong> <br/>
			              <span className="badge badge-secondary">HTML</span>
			              <span className="badge badge-secondary">CSS</span>
			              <span className="badge badge-secondary">Bootstrap</span>
			              <span className="badge badge-secondary">Javascript</span>
			              <span className="badge badge-secondary">Node.js</span>
			              <span className="badge badge-secondary">NPM</span>
			              <span className="badge badge-secondary">Mongoose</span>
			              <span className="badge badge-secondary">Express</span>
			              <span className="badge badge-secondary">Nodemon</span>
			              <span className="badge badge-secondary">MongoDB</span>
			            </p>                  
			            <a className = "my-3 btn btn-block btn-dark btn-sm" href="https://cbcdavid2007.gitlab.io/accounting-ph/" target="_blank" rel="noreferrer">Check project here</a>                
			        </div>
			      </div>
			      <div className = "col imageSide">
			        <a target="_blank" rel="noreferrer" href="https://cbcdavid2007.gitlab.io/accounting-ph/"><img src={project2} className="card-img imageProject" alt="..."/></a> 
			      </div>
			    </div>
			  </div>
			{/*end of Project 2*/}

			{/*start of Project 3*/}
			  <div className = "mt-5">
			    <div className = "row text-center headerProj">
			      <div className = "col-md-12">
			        <h1> Integrated App</h1>
			        <p className="smaller"> <strong> Budget Buddy: </strong>A Budget Tracking App for Every Juan</p>
			        <hr className = "mb-4"/>
			      </div>
			    </div>
			    <div className = "row text-center">
			      <div className = "col-lg-3 side-info2">
			        <div className="container text-left">
			            <p className = "mb-2 mt-2">
			              <strong> Project Title:</strong> Budget Buddy
			            </p>
			            <p className = "mb-2">
			              <strong> Project Description:</strong> A budget tracking app for Filipinos wanting to monitor and visualize their personal and business expenses
			            </p>
			            <p className = "mb-2">
			              <strong>User Features:</strong> <br/>
			              <span className="badge badge-secondary">register</span>
			              <span className="badge badge-secondary">login</span> 
			              <span className="badge badge-secondary">landing</span> <br/>
			              <span className="badge badge-secondary">dashboard</span>
			              <span className="badge badge-secondary">transaction history</span>
			              <span className="badge badge-secondary">add a transaction </span> <br/>
			              <span className="badge badge-secondary">edit/delete a transaction </span> 
			              <span className="badge badge-secondary">account settings</span>
			              <span className="badge badge-secondary">logout</span>
			            </p>
			            <p className = "mb-2">
			              <strong>Tools used:</strong> <br/>
			              <span className="badge badge-secondary">React</span>
			              <span className="badge badge-secondary">Next</span>
			              <span className="badge badge-secondary">Bootstrap</span>
			              <span className="badge badge-secondary">Node.js</span>
			              <span className="badge badge-secondary">NPM</span>
			              <span className="badge badge-secondary">MongoDB</span>
			              <span className="badge badge-secondary">Express</span>
			              <span className="badge badge-secondary">Chart.js</span>
			            </p>                  
			            <a className = "my-3 btn btn-block btn-dark btn-sm" href="https://my-budget-buddy.vercel.app/register" target="_blank" rel="noreferrer">Check project here</a>                
			        </div>
			      </div>
			      <div className = "col imageSide">
			        <a target="_blank" href="https://my-budget-buddy.vercel.app/register" rel="noreferrer"><img src={project3} className="card-img imageProject" alt="..."/></a>
			      </div>
			    </div>
			  </div>
			{/*end of Project 3*/}

			</Container>
		</>
		)
}