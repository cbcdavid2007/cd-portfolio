import React from 'react';

import './App.css';

import Home from './pages/Home'
import AboutMe from './pages/AboutMe'
import MyPortfolio from './pages/MyPortfolio'
import Contact from './pages/Contact'
import Error from './pages/Error';

import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'

// import NavBar from './components/NavBar'
// Route, Switch -> tell what components/pages should be rendered on a specific URL.
/*to comment out*/

function App() {
  return (
    <div className="App">
      	<Router>
      	{/*
	    <NavBar/>
	   	*/} 
	    	<Switch>
	    		{/*use the Route component*/}
	    		<Route exact path = "/" component={Home}/>
	    		<Route exact path = "/aboutMe" component={AboutMe}/>
	    		<Route exact path = "/portfolio" component={MyPortfolio}/>
	    		<Route exact path = "/contactMe" component={Contact}/>
	    		{/*
	    		<Route exact path = "/aboutMe" component={AboutMe}/>
	    		<Route exact path = "/skills" component={Skills}/>
	    		<Route exact path = "/portfolio" component={Portfolio}/>
	    		<Route exact path = "/contactMe" component={ContactMe}/>

	    		<Route exact path = "/register" component={Register}/>
	    		<Route exact path = "/login" component={Login}/>
	    		<Route exact path = "/courses" component={Courses}/>
	    		*/}
	    		<Route component={Error}/>
	    	</Switch>
	    </Router>
    </div>
  );
}

export default App;
